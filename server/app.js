// eslint-disable-next-line @typescript-eslint/no-var-requires
const http = require("http");

const hostname = "127.0.0.1";
const port = 3000;
http
  .createServer((req, res) => {
    res.writeHead(401, {
      "Content-Type": "text/html",
      "Access-Control-Allow-Origin": "*",
    });
    res.end(JSON.stringify({ test: true }));
  })
  .listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
  });
