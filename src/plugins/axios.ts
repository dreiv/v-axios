import { Plugin } from "vue";
import axios from "axios";

const config = {
  // baseURL: process.env.baseURL || process.env.apiUrl || ""
  // timeout: 60 * 1000, // Timeout
  // withCredentials: true, // Check cross-site Access-Control
};
const _axios = axios.create(config);

const axiosPlugin: Plugin = {
  install(app) {
    const { $router } = app.config.globalProperties;

    // Add a response interceptor
    _axios.interceptors.response.use(
      function (response) {
        // Do something with response data
        return response;
      },
      function (error) {
        // Do something with response error
        if (error.response.status === 401) {
          console.log("Unauthorized Request");
          $router.push("unauthorized");
        }

        return Promise.reject(error);
      }
    );

    app.config.globalProperties.$axios = _axios;
  },
};

export default axiosPlugin;
